import { Container } from "@mui/material";
import Image from "next/image";
import graph from '../../../../public/images/barchart.png'
import useTranslation from "next-translate/useTranslation";
import styles from './aboutUs.style.module.scss'




export function BarChart(){

    const { t } = useTranslation("common");

    return (
      <div style={{ padding: "80px 0" }}>
        <Container>
          <div
            className={`header ${styles.barchartHeader}`}
            style={{ marginBottom: "40px" }}
          >
           
            {t("shopCoveregeInformation")}
          </div>
          <div className={styles.barchart}>
          <Image src={graph} />
          </div>
        </Container>
      </div>
    );
}