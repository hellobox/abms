import { Container } from '@mui/material';
import useTranslation from 'next-translate/useTranslation';
import Image from 'next/image';
import Link from 'next/link';
import logo from '../../../../public/images/logo.png';
import styles from './style.module.scss';


export function Footer() {
  const { t } = useTranslation("common");
  return (
    <footer className={styles.footer}>
      <Container>
        <div className={styles.footerInner} id="contact">
          <div className={styles.box}>
            <Link href="/">
              <a className={styles.logo}>
                <div className={styles.logoName}>
                <Image src={logo} alt="logo" width={102} height={46} />
                </div>
              </a>
            </Link>
            <nav>
              <ul>
                <li>
                  <Link href="/">
                    <a>{t("main")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/about">
                    <a>{t("aboutUs")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/#partners">
                    <a>{t("partners")}</a>
                  </Link>
                </li>
                <li>
                  <Link href="/contact">
                    <a>{t("contacts")}</a>
                  </Link>
                </li>
                
              </ul>
            </nav>
          </div>
          <div className={styles.address}>
          {/* <div className={styles.addressText}>
            <div className={styles.phoneIcon}>
              
              <Phone />
            </div>
            <div className={styles.title}>+998 12 345 67 89</div>
           
          </div> */}
          
        </div>
        </div>
       
      </Container>
      {/* <div className={styles.underFooter}>
        © Lumber Land 2022 All rights reserved
      </div> */}
    </footer>
  );
}


