import { Container, Grid } from "@mui/material";
import useTranslation from "next-translate/useTranslation";
import { Carousel } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import styles from "./style.module.scss";
import Image from "next/image";
import { styled } from "@material-ui/styles";


const CustomCarousel = styled(Carousel)({
    "& .carousel-control-prev": {
      top: "41% !important",
      left: "-2% !important",
      boxShadow: '0px 4px 20px rgba(0, 0, 0, 0.08)'
    },
    "& .carousel-control-next":{
        top: "41% !important",
        left: "98% !important",
        boxShadow: '0px 4px 20px rgba(0, 0, 0, 0.08)'
    },
    "& .carousel-inner":{
        overflow: 'inherit'
    },
    "@media(max-width:900px)": {
        "& .carousel-control-next":{
          left: "94% !important",
        }
    },
    "@media(max-width:500px)": {
      "& .carousel-control-next":{
        left: "88% !important",
      }
  }
  });

export function Partners({title, data, background, responsiveData}) {
  const { t } = useTranslation("common");
  const breakPoints = [
    { width: 1, itemsToShow: 1 },
    { width: 550, itemsToShow: 1, itemsToScroll: 2 },
    { width: 768, itemsToShow: 2 },
    { width: 1200, itemsToShow: 3 },
  ];

  return (
    <div id="partners" style={{ background: background, padding: "80px 0" }}>
      <Container breakPoints={breakPoints} style={{overflow: 'hidden'}}>
        <div>
          <div className={`header ${styles.title}`} > {t(`${title}`)} </div>
          <div  className={styles.real}>
          <CustomCarousel indicators={false} interval={4000} controls={ data?.length === 1 ? false : true}>
            {data?.map((item) => (
              <CustomCarousel.Item
                key={item.id}
                className={styles.itemP}
                interval={4000}
              >
                <Grid container spacing={2} style={{justifyContent: 'center'}}>
                  {item?.content?.map((elm) => (
                    <Grid item md={3}>
                      <div className={styles.card}>
                        <Image src={elm?.imageUrl} alt="slides" width={'145px'} height={'64px'} />
                      </div>
                    </Grid>
                  ))}
                </Grid>
              </CustomCarousel.Item>
            ))}
          </CustomCarousel>
          </div>

          <div className={styles.responsiveSection}>
          <CustomCarousel indicators={false} interval={4000} controls={ data?.length === 1 ? false : true}>
            {responsiveData?.map((item) => (
              <CustomCarousel.Item
                key={item.id}
                className={styles.itemP}
                interval={4000}
              >
                <Grid container spacing={2} style={{justifyContent: 'center'}}>
                  {item?.content?.map((elm) => (
                    <Grid item md={3}>
                      <div className={styles.card}>
                        <Image src={elm?.imageUrl} alt="slides" width={'145px'} height={'64px'} />
                      </div>
                    </Grid>
                  ))}
                </Grid>
              </CustomCarousel.Item>
            ))}
          </CustomCarousel>
          </div>

          
        </div>
      </Container>
    </div>
  );
}
