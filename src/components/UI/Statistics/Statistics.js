import { Container, Grid } from '@mui/material'
import useTranslation from 'next-translate/useTranslation'
import React from 'react'
import style from './statistics.module.scss'
import towerImage from '../../../../public/images/tower.png'
import Image from 'next/image'

const Statistics = () => {

  const {t} = useTranslation("common")

  const statData = [
    {
        title: 'Offices',
        count: '5',
        // width: 6
    },
    {
        title: 'Storehouse',
        count: '18+',
        // width: 6
    },
    {
        title: 'Shops',
        count: '9',
        // width: 6
    },
    {
        title: 'Our staff number',
        count: '350+',
        // width: 6
    },
    {
        count: <div> 27 <span style={{fontSize: '23px'}}>000 sq. m</span>  </div>,
        title: 'Total area of our storehouses',
        width: '99%'
    },
   
  ]

  return (
    <div style={{ paddingBottom: "80px" }}>
      <Container>
        <div className={`header ${style.title}`}> {t("nowadaysThereAre")} </div>
        <div className={style.statBlock}>
          <div className={style.leftBlock}>
            {" "}
            <Image src={towerImage} />{" "}
          </div>
          <div className={style.rightBlock}>
            <div className={style.cardSection} container spacing={3}>
              {statData?.map((item) => (
                <div
                  item
                  xs={item?.width}
                  className={style.card}
                  style={{ width: item?.width }}
                >
                  <div className={style.count}> {item?.count} </div>
                  <div className={style.cardstitle}> {item?.title} </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
}

export default Statistics