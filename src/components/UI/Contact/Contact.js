import { Button, Card, Container, IconButton, Modal, TextField } from "@mui/material";
import styles from "./contact.module.scss";
import useTranslation from "next-translate/useTranslation";
import { styled } from "@material-ui/styles";
import { useState } from "react";
import { useRouter } from 'next/router';
import { ErrorIcon, SuccessIcon } from "utils/icons";


const CustomTextfield = styled(TextField)({
  "& .MuiInputBase-input": {
    padding: "10px 12px",
  },
});

export function Contact() {
  const { t } = useTranslation("common");
  const [modal, setModal] = useState(false)
  const [modalText, setModalText] = useState('')
  const router = useRouter()
  const [state, setState] = useState()

  const [values, setValues] = useState({
    name: "",
    email: "",
    message: "",
    phone: ""
  })

  const origin =
  typeof window !== 'undefined' && window.location.origin
      ? window.location.origin
      : '';


  const {name, email, message, phone} = values

  const sendEmail = async (e) => {
    e.preventDefault() 
    try{
      await fetch(`${origin}/api/contact`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(values)
      })
      setModal(true)
      setModalText("Email muvaffaqiyatli jo'natildi.")
    }catch(err){
      setModal(true)
      setModalText("Email jo'natishda xatolik, birozdan so'ng urinib ko'ring!.")
      console.log(err)
    }
  }

  const handleChange = (e) => {
    setValues({...values, [e.target.name]: e.target.value})
  }

  function changeValue(e, type) {
    const value = e.target.value;
    const nextState = {};
    nextState[type] = value;
    setState(nextState);
}

  return (
    <>
      <div className={styles.background}>
        <Container>
          <div className={styles.form}>
            <form onSubmit={sendEmail} method="post">
              <div className={styles.header}> {t("contactUs")} </div>
              <div className={styles.formSection}>
                <CustomTextfield
                  placeholder={t("phoneNumber")}
                  className={styles.inputField}
                  value={phone}
                  name="phone"
                  onChange={handleChange}
                />
                <CustomTextfield
                  placeholder={t("companyName")}
                  className={styles.inputField}
                  value={name}
                  name="name"
                  onChange={handleChange}
                />
                <CustomTextfield
                  placeholder={t("email")}
                  className={styles.inputField}
                  value={email}
                  name="email"
                  onChange={handleChange}
                  type="email"
                  // onChange={e => changeValue(e, 'email')}
                  
                />
                <textarea
                  placeholder={t("message")}
                  value={message}
                  name="message"
                  onChange={handleChange}
                  // onChange={(e) => setMessage(e.target.value)}
                />
                <div className={styles.button}>
                  <Button type="submit"> {t("send")} </Button>
                </div>
              </div>
            </form>
          </div>
        </Container>
      </div>
      <Modal open={modal} onClose={setModal} className={styles.modal}>
        <Card className={styles.card}>
          <div className={styles.header}>
            <IconButton
              className={styles.closeButton}
              onClick={() => {setModal(false); router.push("/")}}
            >
              x
            </IconButton>
          </div>

          <div className={styles.body}>
           <div className={styles.icon}>
           {modalText === "Email muvaffaqiyatli jo'natildi." ? <SuccessIcon /> : <ErrorIcon /> } 
           </div>
            <div className={styles.bodyText}>
              {modalText}
            </div>
          </div>
        </Card>
      </Modal>
    </>
  );
}
