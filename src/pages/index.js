import SEO from 'components/SEO'
import { Main } from 'components/UI/Main/Main'
import { Brands } from 'components/UI/Brands/Brands'
import { Network } from 'components/UI/Network/Network'
import { OurBenefits } from 'components/UI/OurBenefits/Benefits'
import { Partners } from 'components/UI/Partners/Partners'
import Statistics from 'components/UI/Statistics/Statistics'
import { companyPartners, homePartners, ourPartners, responsiveCompanyPartners, responsiveHomePartners } from 'constants/partners'


export default function Home({ data }) {
  
  return (
    <>
      <SEO />
      <Main />
      <Brands />
      <Partners title={"homeApplianceCompany"} data={homePartners} background={"#D9D9D9"} responsiveData={responsiveHomePartners} />
      <Statistics /> 
      <Partners title={"itCompanyPartners"} data={companyPartners} background={"#D9D9D9"} responsiveData={responsiveCompanyPartners} />
      <Partners title={"ourPartners"} data={ourPartners} background={"white"} responsiveData={ourPartners} />
      <OurBenefits />
      <Network />
    </>
  )
}

