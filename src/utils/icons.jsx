export const RuFLag = () => (
  <svg
    width="20"
    height="20"
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect
      x="0.25"
      y="0.25"
      width="19.5"
      height="19.5"
      rx="9.75"
      fill="white"
      stroke="#F5F5F5"
      strokeWidth="0.5"
    />
    <mask
      id="mask0_2366_22665"
      style="mask-type:luminance"
      maskUnits="userSpaceOnUse"
      x="0"
      y="0"
      width="20"
      height="20"
    >
      <rect
        x="0.25"
        y="0.25"
        width="19.5"
        height="19.5"
        rx="9.75"
        fill="white"
        stroke="white"
        strokeWidth="0.5"
      />
    </mask>
    <g mask="url(#mask0_2366_22665)">
      <path d="M0 13.3334H20V6.66675H0V13.3334Z" fill="#0C47B7" />
      <path d="M0 19.9999H20V13.3333H0V19.9999Z" fill="#E53B35" />
    </g>
  </svg>
);



export const SuccessIcon = () => (
  <svg
    width="96"
    height="96"
    viewBox="0 0 96 96"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <rect width="96" height="96" rx="48" fill="#4CD964" />
    <path
      d="M27 48L42 63L72 33"
      stroke="white"
      stroke-width="9"
      stroke-linecap="round"
      stroke-linejoin="round"
    />
  </svg>
);


export const ErrorIcon = () => (
  <svg
    width="96"
    height="96"
    viewBox="0 0 96 96"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M0 48C0 21.4903 21.4903 0 48 0V0C74.5097 0 96 21.4903 96 48V48C96 74.5097 74.5097 96 48 96V96C21.4903 96 0 74.5097 0 48V48Z"
      fill="#F2271C"
    />
    <path
      d="M76 25.64L70.36 20L48 42.36L25.64 20L20 25.64L42.36 48L20 70.36L25.64 76L48 53.64L70.36 76L76 70.36L53.64 48L76 25.64Z"
      fill="white"
    />
  </svg>
);